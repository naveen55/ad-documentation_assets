---
type: page
title: Asset Details Profile Tab
listed: true
slug: asset-details-profile-tab
description: 
index_title: Asset Details Profile Tab
hidden: 
---draft

Asset profiling serves as an essential precursor to any data quality enhancement initiative, as it enables organizations to better understand the current state of their data, recognize vulnerabilities, and take informed actions to rectify issues and optimize their data assets for more accurate and reliable analytics and decision-making. ADOC provides the capability to perform data profiling not only for structured data but also for semi-structured data, enabling users to gain valuable insights from both types of data sources.

The **Profile** tab displays the following information about the assets profiled within the selected table, for a selected date and time.

| Property Name | Definition | Example | 
| ---- | ---- | ---- | 
| **Executed Profile** | Defines the most recent date and time at which the profiling of asset occurred. Click the drop-down and select a date and time  to view previous profile executions details. | Aug 24, 2023 8:26pm | 
| **Rows Profiled** | Number of rows profiled. | 2976508 | 
| **Profiling Type** | Full or Sample type of asset profiling? | FULL | 
| **Start Time** | Defines the date and time at which the profiling of the asset started. | Aug 24, 2023 8:26pm | 
| **End Time** | Defines the date and time at which the profiling had ended. | Aug 24, 2023 8:27pm | 
| **Start Value** | Defines the value with which the profiling began. | 169271...114824 | 
| **End Value** | Defines the value with which the profiling completed. | 169288...763048 | 


- **Compare Profiles**: Click on the shuffle icon to compare the current profiled data of an asset with previously profiled data.

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/t8yq4ej6ixa54llquw0dtde0xy30d5dxmsq1dvzfh3oc814k0pi2kch5gwxsqsib.png",
        "mode": "responsive",
        "width": 3074,
        "height": 262,
        "caption": null
    }
}]$

### Profiling an Asset

To start profiling, click the **Action** button and then select either **Full Profile,** **Incremental, or Selective** from under **Profile.**

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/hls592m6z5xtzouzbnnmjwrbf3ysiaktw5d4w6ld2ttqenyu8fyirs9jk5la3vn5.png",
        "mode": "responsive",
        "width": 502,
        "height": 756,
        "caption": "Action Button"
    }
}]$

Once the profiling is completed, a table is generated with names of each of the columns present in the table. Various metrics are calculated for each column. Each column contains one data type and the metrics generated for a structured column data types are as follows:

$plugin[{
    "type": "tab-block",
    "data": {
        "tabs": [
            {
                "title": "String",
                "plugins": [],
                "contents": "Metrics calculated for the string data type are:\n\n1. **Not Nulls**- Completeness of data i.e., if there are any null values in the column.\n2. **Distinct**- Dissimilarity in the data.\n3. **Min Len**- Minimum number of characters.\n4. **Avg Len**- Average number of characters.\n5. **Max Len**- Maximum number of characters.\n6. **Case Count**- Number of lower case, upper case and mixed case characters."
            },
            {
                "title": "Integral",
                "plugins": [],
                "contents": "Metrics calculated for the integral data type are:\n\n1. **Not Nulls**- Completeness of data i.e., if there are any null values in the column.\n2. **Distinct**- Dissimilarity in the data.\n3. **Min**- Minimum value of an integer in the column.\n4. **Mean**- Average value of the integral data.\n5. **Max**- Maximum value of an integer in the column.\n6. **StdDev**- Standard deviation of data in the column."
            },
            {
                "title": "Fractional",
                "plugins": [],
                "contents": "Metrics calculated for the fractional data type are:\n\n1. **Not Nulls**- Completeness of data i.e., if there are any null values in the column.\n2. **Distinct**- Dissimilarity in the data.\n3. **Min**- Minimum value of a fraction in the column.\n4. **Mean**- Average value of the fractional data.\n5. **Max**- Maximum value of a fraction in the column.\n6. **StdDev**- Standard deviation of data in the column."
            },
            {
                "title": "Time Stamp",
                "plugins": [],
                "contents": "Metrics calculated for the timestamp data type are:\n\n1. **Not Nulls**- Completeness of data i.e., if there are any null values in the column.\n2. **Distinct**- Dissimilarity in the data."
            },
            {
                "title": "Boolean",
                "plugins": [],
                "contents": "Metrics calculated for the Boolean data type are:\n\n1. **Not Nulls**- Completeness of data i.e., if there are any null values in the column.\n2. **Distinct**- Dissimilarity in the data."
            }
        ]
    }
}]$

Similarly, the metrics generated for semi-structured column data types are as follows:

$plugin[{
    "type": "tab-block",
    "data": {
        "tabs": [
            {
                "title": "Struct",
                "plugins": [],
                "contents": "Metrics calculated for the Struct data type are:\n\n1. **% Not Nulls:** Completes of data i.e., if there are any null values in the column.\n2. **Distinct:** Dissimilarity in the data.\n3. **Min Keys:** Minimum number of keys(fields).\n4. **Max Keys:** Maximum number of keys(fields).\n5. **Avg Keys:** Average number of keys(fields)."
            },
            {
                "title": "Array[String]",
                "plugins": [],
                "contents": "Metrics calculated for an array of string data type are:\n\n1. **% Not Nulls**: Percentage of non-null values in the array.\n2. **Distinct**: Number of unique values in the array.\n3. **Min Array Length**: Minimum number of elements in the array.\n4. **Max Array Length**: Maximum number of elements in the array.\n5. **Avg Array Length**: Average number of elements in the array.\n6. **Min Length**: Minimum length of individual string elements in the array.\n7. **Max Length**: Maximum length of individual string elements in the array.\n8. **Avg Length**: Average length of individual string elements in the array.\n9. **Pattern**: Common patterns found in the string elements of the array.\n10. **Top Values**: Most frequently occurring values in the array."
            },
            {
                "title": "Array[Integral\/Fractional]",
                "plugins": [],
                "contents": "Metrics calculated for an array of integral or fractional data type are:\n\n1. **% Not Nulls**: Percentage of non-null values in the array.\n2. **Distinct**: Number of unique values in the array.\n3. **Min Array Length**: Minimum number of elements in the array.\n4. **Max Array Length**: Maximum number of elements in the array.\n5. **Avg Array Length**: Average number of elements in the array.\n6. **Min Value**: Smallest value in the array of integral or fractional type.\n7. **Max Value**: Largest value in the array of integral or fractional type.\n8. **Avg Value**: Average value of elements in the array.\n9. **Pattern**:  Common patterns found in the numeric elements of the array.\n10. **Top Values**: Most frequently occurring values in the array."
            },
            {
                "title": "Array[Boolean]",
                "plugins": [],
                "contents": "Metrics calculated for an array of string data type are:\n\n1. **% Not Nulls:** Percentage of non-null values in the array.\n2. **Distinct:** Number of unique values in the array.\n3. **Min Array Length:** Minimum number of elements in the array.\n4. **Max Array Length**: Maximum number of elements in the array.\n5. **Avg Array Length**: Average number of elements in the array.\n6. **Top Values:** Most frequently occurring values in the array."
            },
            {
                "title": "Array[Struct]",
                "plugins": [],
                "contents": "Metrics calculated for an array of string data type are:\n\n1. **% Not Nulls:** Percentage of non-null arrays (structs) within the array.\n2. **Distinct:** Number of unique array (struct) values in the array.\n3. **Min Array Length:** Minimum number of arrays (structs) in the array.\n4. **Max Array Length:** Maximum number of arrays (structs) in the array.\n5. **Avg Array Length**: Average number of arrays (structs) in the array.\n6. **Min Keys**: Minimum number of keys (fields) present in the structs within the array.\n7. **Max Keys**: Maximum number of keys (fields) present in the structs within the array.\n8. **Avg Keys**: Average number of keys (fields) present in the structs within the array."
            }
        ]
    }
}]$

### Viewing Column Data Insights

To gain deeper insights into any column type, whether structured or not, simply click on the column name. This action will open a modal window presenting the following details:

$inline[badge,Info,info] If a column is semi-structured, like an Array[Struct] or Struct data type, you can gain insights into its sub-column data by clicking on the **Expand** button located beneath the column name. This action allows you to delve into the details of the nested components, enhancing your understanding of the complex data structure. 

$plugin[{
    "type": "custom-html",
    "data": {
        "contents": "<video src=\"https:\/\/gitlab.com\/naveen55\/ad-documentation_assets\/-\/raw\/main\/Documentation%20Videos\/profiling_assets.mov\" width=\"100%\" preload controls=\"none\" type=\"video\/mov\" ><\/video>"
    }
}]$

#### Column Statistics

This section provides a table showcasing statistics for the selected column, accompanied by a bar graph illustrating percentage-based evaluations like % Null values and % Unique values.

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/wn5i1grndj5xli6d4x3psu95na3ugj9yotlsa4kgbtzw2qs98zws2j7w3lvyntgu.png",
        "mode": "responsive",
        "width": 2258,
        "height": 1222,
        "caption": null
    }
}]$

#### Most Frequent Values

This section provides a list of the most frequent values found for the selected column.

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/1l3rh2ly2ixivx7n2i82grxpzl9r83bdfq7zeqmlk85vm9op0m1juzhboxhwia45.png",
        "mode": "responsive",
        "width": 2258,
        "height": 1296,
        "caption": null
    }
}]$

#### Detected Patterns

This section provides a list of common patterns found for the selected column.

#### Anomalies & Trends

Within this section, you'll find a variety of charts that offer valuable insights into your data. These visualizations present key metrics such as skewness, distinct count, completeness, and kurtosis. These charts help you understand the distribution and patterns within your data, enabling you to identify potential anomalies and trends that may influence your analysis and decision-making processes.

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/xw0wsnyuf7jhihtjlpdjmxrw47p86jjxygzam75lma5kv7i9cg50gofqm35ql1va.png",
        "mode": "responsive",
        "width": 2262,
        "height": 1846,
        "caption": null
    }
}]$

---published

Asset profiling helps to identify data quality issues in data sources. The **Profile** tab displays the following information about the assets profiled within the selected table, for a selected date and time.

| Property Name | Definition | Example | 
| ---- | ---- | ---- | 
| **Rows Profiled** | Number of rows profiled. | 2976508 | 
| **Profiling Type** | Full or Sample type of asset profiling? | FULL | 
| **Start Time** | Defines the date and time at which the profiling of the asset started. | 2021-09-23 03:30:00 | 
| **End Time** | Defines the date and time at which the profiling had ended. | 2021-09-23 03:30:47 | 


- Click the drop-down menu and select a date and time to view profile information for the selected table.
- **Compare Profiles**: Click on the shuffle icon to compare the current profiled data of an asset with previously profiled data.

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/8jco1ls2ynacszjmdo9f47ebe2gtz928hilol02lb690jh91tkn3ojs25n7qiypk.png",
        "mode": "responsive",
        "width": 1471,
        "height": 146,
        "caption": null
    }
}]$

### Profiling an Asset

To start profiling, click the **Action** tab, select **Profile** from the drop-down menu. Then, select **Full Profile** or **Increment Profile.**

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/vus0emnw1tsojy4jilxm5qzoqhrlnejpgx3lsicqshgbo1jl06jss6obm8md18x0.png",
        "mode": "responsive",
        "width": 425,
        "height": 367,
        "caption": "Action Button"
    }
}]$

Once the profiling is completed, charts are generated related to each of the columns present in the table. Various metrics are calculated for each column. Each column contains one data type and the metrics generated for different data types are as follows:

1. **STRING**- Metrics calculated for the string data type are:
    1. **Not Nulls**- Completeness of data i.e., if there are any null values in the column.
    2. **Distinct**- Dissimilarity in the data.
    3. **Min Len**- Minimum number of characters.
    4. **Avg Len**- Average number of characters.
    5. **Max Len**- Maximum number of characters.
    6. **Case Count**- Number of lower case, upper case and mixed case characters.

2. **INTEGRAL-** Metrics calculated for the integral data type are:
    1. **Not Nulls**- Completeness of data i.e., if there are any null values in the column.
    2. **Distinct**- Dissimilarity in the data.
    3. **Min**- Minimum value of an integer in the column.
    4. **Mean**- Average value of the integral data.
    5. **Max**- Maximum value of an integer in the column.
    6. **StdDev**- Standard deviation of data in the column.

3. **FRACTIONAL**- Metrics calculated for the fractional data type are:
    1. **Not Nulls**- Completeness of data i.e., if there are any null values in the column.
    2. **Distinct**- Dissimilarity in the data.
    3. **Min**- Minimum value of a fraction in the column.
    4. **Mean**- Average value of the fractional data.
    5. **Max**- Maximum value of a fraction in the column.
    6. **StdDev**- Standard deviation of data in the column.

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/9408w56zgwpm38ad3vqgggqh0a3benucxqzmiu19t5al24qxk4un4ntzfrwyaqmv.png",
        "mode": "responsive",
        "width": 423,
        "height": 361,
        "caption": null
    }
}]$

4. **TIMESTAMP**- Metrics calculated for the timestamp data type are:
    1. **Not Nulls**- Completeness of data i.e., if there are any null values in the column.
    2. **Distinct**- Dissimilarity in the data.

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/tkjcdmrroktryyv5cehcbj211aa6qu1zlhwi08sdj6e9mfk25xb6ygafnet80ik9.png",
        "mode": "responsive",
        "width": 1032,
        "height": 526,
        "caption": null
    }
}]$

5. **BOOLEAN**- Metrics calculated for the Boolean data type are:
    1. **Not Nulls**- Completeness of data i.e., if there are any null values in the column.
    2. **Distinct**- Dissimilarity in the data.

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/fokw2chqtsekqkv8f1398szotwsc6v9av4iaonvoj4jhqetz85sc3gxlirwhf10v.png",
        "mode": "responsive",
        "width": 895,
        "height": 468,
        "caption": null
    }
}]$

## Asset Panel

The asset detail view has a panel at the left. This panel displays important information about the asset and also allows you to perform few tasks on the assets. The various items available in the left panel are described below.

### Analysis

This section describes the status of asset profiling and schema updation. If the asset is profiled, you can see how many days or minutes ago the asset was profiled. If an asset is never profiled, this section displays a message as "Profiled details not available". Similarly, this section also displays how many days or minutes ago the asset schema was updated. 

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/80japglee2w9gyy6u71uow6tswis6qqalxdzfv9j329ugycjhyvrp4livlzc9fjb.png",
        "mode": "responsive",
        "width": 565,
        "height": 325,
        "caption": null
    }
}]$

The following images displays the Analysis section of an unprofiled asset. 

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/x1uybygoh5e3a671swzl3v495uxcz38ae7m2eynvfqq1p9abp22euaydkexdm4p2.png",
        "mode": "responsive",
        "width": 895,
        "height": 355,
        "caption": null
    }
}]$

### Quality Status

This section displays the status of various policy types (data quality, reconciliation, data drift, or schema drift), defined on an asset. Apart from the status of the policies defined, you can also view if there are any anomalies detected in the asset profile. The anomaly detection feature is only applicable for profiled assets. If a policy type is not defined on an asset, you can view a message that the policy type is not defined on the asset. 

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/akhbw9at761ms8ypan7a9tme2hv39pa04grc867r4wbcavyw0lp2fyyr75eb0lnb.png",
        "mode": "responsive",
        "width": 502,
        "height": 552,
        "caption": null
    }
}]$

### Tags

This section displays the list of tags created on the asset. You can add tags by clicking the **Add Tag** button. You can use the Tags to create **Tags Match** rules in Rules. 

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/4ghdirl4au0iybsl8e7bncmdb1zn8kzstg7pqfpaijmveqmd7ueiupjxrjaixsg2.png",
        "mode": "responsive",
        "width": 463,
        "height": 591,
        "caption": null
    }
}]$

### Associated Terms

You can add a list of associated terms to an asset. These terms can be words which can help you or anyone from your organization to easily identify asset. For example, you can add a term critical to all the table assets that are highly critical for your business. 

### Description

You can describe an asset in your own words in this section. A description can be a phrase, sentence or a small paragraph about the asset. 

$plugin[{
    "type": "image",
    "data": {
        "url": "https:\/\/uploads.developerhub.io\/prod\/Yoq2\/g6ixps2pkv8p16ahm57rc2viwp90v3ghjc19crrcizw8qo52a344z239fa9jz1u1.png",
        "mode": "responsive",
        "width": 409,
        "height": 217,
        "caption": null
    }
}]$

### Segments

To learn about Segments and the process of creating segments, refer to the [Segmented Analysis](https://docs.acceldata.io/data-observability-cloud/documentation/segmented-analysis) document.

